import os

def list_dat_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('.dat')]
    return files

def make_new_dir(dirpath):
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    return dirpath

def read_dat_file(dirpath, dat_file):
    with open(os.path.join(dirpath, dat_file), 'r') as datfile:
        dat_data = datfile.read().splitlines()

    return dat_data

def write_csv_from_dat(datfile, datdata, dirpath):
    csvfname = str(datfile).split('.')[0] + '.csv'
    with open(os.path.join(dirpath, csvfname), 'w') as csvfile:
        header = '#, \n'
        csvfile.write(header)
        for item in datdata:
            split_fields = item.split()
            join_fields = (',').join(split_fields)+', \n'
            csvfile.write(join_fields)
        csvfile.close()

if __name__=='__main__':
    dat_dir_location = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\082018"
    dat_files = list_dat_files(dat_dir_location)
    output_location = os.path.join(dat_dir_location, 'csv_output')
    outputpath = make_new_dir(output_location)
    for file in dat_files:
        datdata = read_dat_file(dat_dir_location, file)
        print('heho')
        write_csv_from_dat(file, datdata, outputpath)