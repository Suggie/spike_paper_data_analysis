import os
import numpy as np
import matplotlib.pyplot as plt

dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\082018\csv_output"
fname = "broo_G2_3cosfield_cyto7_spike_trace_norelax_300mps.csv"

data = np.genfromtxt(os.path.join(dirpath, fname), delimiter=',', skip_header=1)

plt.plot(data[:, 0], data[:, 1], ls='-', marker='+', color='black')
# plt.xlim((23, 28))
# plt.ylim((13, 22))
plt.savefig(os.path.join(dirpath, 'dt_vs_TW_plateau.pdf'))
plt.close()