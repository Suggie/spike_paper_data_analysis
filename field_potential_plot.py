import os
import numpy as np
import matplotlib.pyplot as plt



def potential(x):
    arr = np.multiply(x, np.pi/6)
    y = np.cos(arr)
    return y

def field(x):
    arr = np.multiply(x, np.pi/6)
    y = np.sin(arr)
    return y

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\082018\csv_output"
    x = np.linspace(0, 12, 100)
    pot = potential(x)
    efield = field(x)
    plt.plot(x, pot, color='green', label='potential')
    plt.plot(x, efield, color='blue', label='field')
    plt.legend(loc='best', fontsize='small')
    plt.savefig(os.path.join(dirpath, 'field_potential_plot.pdf'))
    plt.close()