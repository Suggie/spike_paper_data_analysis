import os
import numpy as np
import matplotlib.pyplot as plt
from resolution_exp_plots import gamma



if __name__=="__main__":
    dirpath = r"C:\Users\sugyan\Documents\Processed data\082018_CalData\_calfiles\New_cal_scheme"
    fname_blac_exp = r"C:\Users\sugyan\Documents\Processed data\082018_CalData\_calfiles\New_cal_scheme\blac8_unk\ccs_error_powerfit_powerfitexp_False_unk_output_#b-lactoglobulin_1_8.csv"
    fname_cytc_exp = r"C:\Users\sugyan\Documents\Processed data\082018_CalData\_calfiles\New_cal_scheme\cytc6_unk\ccs_error_powerfit_powerfitexp_False_unk_output_#cytochrome-c_1_6.csv"
    fname_r2_exp = r"C:\Users\sugyan\Documents\Processed data\082018_CalData\_calfiles\New_cal_scheme\All_ions_include\powerfit_powerfitexp_False_cal_output.csv"

    fname_blac_sim = r"C:\Users\sugyan\Documents\Papers\051318_SpikesPaper_withWaters\linear_ccs_err_natlactomono_z8.csv"
    fname_cytc_sim = r"C:\Users\sugyan\Documents\Papers\051318_SpikesPaper_withWaters\linear_ccs_err_natcyto_z6.csv"

    cytc_mobility = 0.02622906
    blac_mobility = 0.02565920

    wavenumber = 524

    blac_data = np.genfromtxt(fname_blac_exp, delimiter=',', skip_header=1)
    cytc_data = np.genfromtxt(fname_cytc_exp, delimiter=',', skip_header=1)
    r2_data = np.genfromtxt(fname_r2_exp, delimiter=',', skip_header=1)

    blac_sim_data = np.genfromtxt(os.path.join(dirpath, fname_blac_sim), delimiter=',', skip_header=1)
    cytc_sim_data = np.genfromtxt(os.path.join(dirpath, fname_cytc_sim), delimiter=',', skip_header=1)


    KVv_blac_exp = blac_mobility * blac_data[:, 2]
    KVv_cytc_exp = cytc_mobility * cytc_data[:, 2]
    KVv_blac_sim = np.multiply(blac_sim_data[:, 7], blac_sim_data[:, 2])
    KVv_cytc_sim = np.multiply(cytc_sim_data[:, 7], cytc_sim_data[:, 2])

    gamm_blac_exp = gamma(wavenumber, blac_data[:, 0], blac_data[:, 1], blac_mobility)
    gamm_cytc_exp = gamma(wavenumber, cytc_data[:, 0], cytc_data[:, 1], cytc_mobility)

    gamm_blac_sim = gamma(wavenumber, blac_sim_data[:, 6], blac_sim_data[:, 5], blac_sim_data[:, 2])
    gamm_cytc_sim = gamma(wavenumber, cytc_sim_data[:, 6], cytc_sim_data[:, 5], cytc_sim_data[:, 2])



    # plt.scatter(KVv_blac_exp,  blac_data[:, 10], color='dodgerblue', edgecolors='black', label='blac_8+', marker='o')
    # plt.scatter(KVv_cytc_exp,  cytc_data[:, 10], color='forestgreen', edgecolors='black', label='cytc_6+', marker='o')
    #
    # plt.plot(KVv_blac_sim,  blac_sim_data[:, 4], color='dodgerblue', ls='--', label='blac_8+_sim')
    # plt.plot(KVv_cytc_sim,  cytc_sim_data[:, 4], color='forestgreen', ls='--', label='cytc_6+_sim')
    # # plt.xlim((0.04, 0.15))
    # # plt.xticks([0.04, 0.06, 0.08, 0.10, 0.12, 0.14])
    # plt.xlim((np.min(KVv_blac_sim)-0.0005, np.max(KVv_blac_sim)+0.0005))
    # plt.legend(loc='best', fontsize='small')
    # plt.xlabel('K*WH/WV')
    # plt.ylabel('%ccs delta')
    # plt.savefig(os.path.join(dirpath, 'delta_ccs_KVv_blac_cytc_exp_sim_spike.pdf'))
    # plt.close()

    plt.figure(figsize=(3.25, 3.5))

    plt.scatter(gamm_cytc_exp, r2_data[:, -2], color='black', s=50)
    # plt.xticks([0.04, 0.06, 0.08, 0.10, 0.12, 0.14])
    # plt.xlim((0.04, 0.15))
    plt.ylim((0.95, 1.00))
    # plt.figure(figsize=(3.25, 3.5), dpi=500,clear=True)
    plt.savefig(os.path.join(dirpath, 'r2_ruotoloall_spike_vs_gamma.pdf'))
    plt.close()

    plt.scatter(gamm_cytc_exp, r2_data[:, -1], color='black', s=50)
    # plt.xticks([0.04, 0.06, 0.08, 0.10, 0.12, 0.14])
    # plt.xlim((0.04, 0.15))
    # plt.ylim((0.95, 1.00))
    plt.savefig(os.path.join(dirpath, 'rmse_ruotoloall_spike_vs_gamma.pdf'))
    plt.close()

    plt.figure(figsize=(3.25, 3.5))

    plt.scatter(gamm_blac_exp, blac_data[:, -1], color='dodgerblue', edgecolors='black', label='blac_8+', marker='o', s=50, linewidth=2)
    plt.scatter(gamm_cytc_exp, cytc_data[:, -1], color='forestgreen', edgecolors='black', label='cytc_6+', marker='o', s=50, linewidth=2)

    plt.plot(gamm_blac_sim, blac_sim_data[:, 4], color='dodgerblue', ls='--', label='blac_8+_sim', linewidth=2)
    plt.plot(gamm_cytc_sim, cytc_sim_data[:, 4], color='forestgreen', ls='--', label='cytc_6+_sim', linewidth=2)
    # plt.xlim((0.04, 0.15))
    # plt.xticks([0.04, 0.06, 0.08, 0.10, 0.12, 0.14])
    # plt.xlim((np.min(KVv_blac_sim) - 0.0005, np.max(KVv_blac_sim) + 0.0005))
    plt.legend(loc='best', fontsize='small')
    plt.xlabel('Gamma')
    plt.ylabel('%ccs delta')

    plt.savefig(os.path.join(dirpath, 'delta_ccs_gamma_blac_cytc_exp_sim_spike.pdf'))
    plt.close()