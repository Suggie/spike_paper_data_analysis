import os
import numpy as np
import matplotlib.pyplot as plt

dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\csv_output"

m_arr = np.arange(5, 50, 1)
n_val = 4

gamma = np.sqrt(1 - np.divide(n_val**2, np.square(m_arr)))

for val in gamma:
    plt.axvline(x=val, color='black')
plt.savefig(os.path.join(dirpath, 'spike_predict_pos.pdf'))
plt.close()