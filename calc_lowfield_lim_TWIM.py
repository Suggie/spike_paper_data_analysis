import os
import EoverN_calculate
import numpy as np
import matplotlib.pyplot as plt
import itertools

def write_gamma_output(store_arr, dirpath, fname):
    with open(os.path.join(dirpath, fname), 'w') as outfile:
        outline = ''
        header = 'mass_of_gas, wavenumber, temp, mobility, wh, wv, gamma_exp, gamma_low_field_limit, gamma_deviation\n'
        outline += header

        for ind in range(len(store_arr[:, 0])):
            vals = ','.join(str(x) for x in store_arr[ind])
            line = '{}\n'.format(vals)
            outline += line

        outfile.write(outline)
        outfile.close()

def plot_hist_gamma_deviation(store_arr, dirpath, fname):
    gamma_dev = store_arr[:, -1]
    plt.hist(gamma_dev, color='black', alpha=0.5)
    plt.xlabel('y_exp - y_low_field_limit')
    plt.ylabel('Count')
    plt.savefig(os.path.join(dirpath, fname+'_.pdf'))
    plt.close()


if __name__=='__main__':

    dirpath = r'C:\Users\sugyan\Documents\Papers\051318_SpikesPaper_withWaters'
    blac_file = 'cytc_1_6_calcouput_ruotoloetal_dt_combined_WH_WV.csv'
    mobility = 0.02330898
    temp_kelvin = 300
    mass_of_gas = 28
    wavenumber = 524

    data = np.genfromtxt(os.path.join(dirpath, blac_file), delimiter=',', skip_header=1)
    store_arr = []

    outfile_name = 'ubq_gamma_lowfield_limit_for_spike_resolution.csv'

    wave_velocity_range = np.arange(200, 601, 20)
    wave_height_range = np.arange(20, 41, 1)

    wave_ht_vel_comb = list(itertools.product(wave_height_range, wave_velocity_range))

    for ind in range(len(wave_ht_vel_comb)):
        wv_ = float(wave_ht_vel_comb[ind][1])
        wh_ = float(wave_ht_vel_comb[ind][0])
        gamma_lowfield_lim = EoverN_calculate.calculate_gamma_low_field(temp_kelvin, mass_of_gas, wv_)
        gamma_exp = EoverN_calculate.gamma_exp(wavenumber, wh_, wv_, mobility)
        gamma_deviation = gamma_exp - gamma_lowfield_lim
        store_arr.append([mass_of_gas, wavenumber, temp_kelvin, mobility, wh_, wv_, gamma_exp, gamma_lowfield_lim, gamma_deviation])

    store_arr = np.array(store_arr)

    write_gamma_output(store_arr, dirpath, outfile_name)
    plot_hist_gamma_deviation(store_arr, dirpath, outfile_name)


    print('heho')