from math import sqrt, pi

def convert_mass_da_to_kg(mass):
    return mass/6.022e26

def convert_charge_to_coulomb(charge):
    return charge*1.6e-19

def convert_ccs_angstrom_to_cm2(ccs):
    return ccs*1.0e-16

def get_diameter_from_ccs(ccs):
    return sqrt(ccs/pi)

def calculate_E_N_low_field(temp_kelvin, mass_of_gas_dalton, radius_of_gas_cm, mass_of_ion_dalton, charge_state_of_ion, ccs_of_ion_angstrom_sq):
    mass_of_gas = convert_mass_da_to_kg(mass_of_gas_dalton)
    mass_of_ion = convert_mass_da_to_kg(mass_of_ion_dalton)
    charge_of_ion = convert_charge_to_coulomb(charge_state_of_ion)
    ccs_of_ion = convert_ccs_angstrom_to_cm2(ccs_of_ion_angstrom_sq)
    diameter_from_ccs = get_diameter_from_ccs(ccs_of_ion)
    radius_of_ion = (diameter_from_ccs - radius_of_gas_cm)*2
    eovern = 1.0e17*3*1.38e-23*temp_kelvin*pi*(radius_of_ion**2)*sqrt(mass_of_ion/(mass_of_ion+mass_of_gas))*(1/charge_of_ion)
    # eovern = 2.89*sqrt(mass_of_ion/(mass_of_ion+mass_of_gas))*(1/charge_of_ion)*((2*radius_of_ion)**2)
    return eovern

def calculate_gamma_low_field(temp_kelvin, mass_of_gas_dalton, wave_velocity):
    mass_of_gas = convert_mass_da_to_kg(mass_of_gas_dalton)
    gamma = (1/wave_velocity)*sqrt((3*1.38e-23*temp_kelvin)/mass_of_gas)
    return gamma

def gamma_exp(wavenumber, waveheight, wavevelocity, mobility):
    potential_amp = (16.67/40)*waveheight
    y = (wavenumber * mobility * potential_amp)/wavevelocity
    return y

if __name__=='__main__':
    mass_of_gas = 28
    radius_of_gas = 3.7e-8
    mass_of_ion = 18363
    charge_state_of_ion = 7
    ccs_of_ion_angs_sq = 1660