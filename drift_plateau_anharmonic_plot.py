import os
import numpy as np
import matplotlib.pyplot as plt

dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\csv_output"
fname = 'plateau_width_300mps_sineterms.csv'

data = np.genfromtxt(os.path.join(dirpath, fname), delimiter=',', skip_header=9)
plateau_width = np.multiply(100, np.divide(data[:, 1], data[:, 2]))
plt.plot(data[:, 0], plateau_width, ls='-', marker='o', markersize=8, mec='black', mfc='none', mew=1.75, color='black')
# plt.xlim((0, 2))
# plt.ylim((0, 11))
plt.savefig(os.path.join(dirpath, 'dt_plateau_anharmonic_2.pdf'))
plt.close()