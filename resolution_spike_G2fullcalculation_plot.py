import os
import numpy as np
import matplotlib.pyplot as plt

def list_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith('total_peaks_')]
    files = [x for x in files if x.endswith('mps.csv')]
    return files

def get_data(dirpath, file):
    data = np.genfromtxt(os.path.join(dirpath, file), delimiter=',', skip_header=1)
    return data

def plot_data(data_x, data_y, label):
    plt.plot(data_x, data_y, label=label)
    plt.legend(loc='best', fontsize='small')

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\csv_output"
    files = list_files(dirpath)
    # dt_arr = []
    # gamma_arr = []
    for file in files:
        label = ('_').join(str(str(file).split('.')[0]).split('_')[4:6])
        data = get_data(dirpath, file)
        plot_data(data[:, 9], data[:, 7], label)
    plt.xlabel('gamma')
    plt.ylabel('t/dt')
    plt.savefig(os.path.join(dirpath, 'resolution_spike_G2fullcalculation_all.pdf'))
    plt.close()