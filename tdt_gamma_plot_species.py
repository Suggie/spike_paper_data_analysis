import os
import numpy as np
import matplotlib.pyplot as plt

# put the file name with the dir location where the data is

def get_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith('total_peaks')]
    return files

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\081518\csv_output"

    files = get_files(dirpath)
    print(files)
    color = ['dodgerblue', 'forestgreen', 'orange']

    for ind, file in enumerate(files):
        file_id = str(file).split('_')[-3]
        print(file_id)
        data = np.genfromtxt(os.path.join(dirpath, file), delimiter=',', skip_header=1)
        gamma = data[:, 9]
        toverdt = data[:, 7]

        plt.plot(gamma, toverdt, color=color[ind], label=file_id, marker='o', markersize=8, markeredgecolor='black', linestyle='-')
        plt.legend(loc='best', fontsize='small')
    plt.xlabel('gamma')
    plt.ylabel('t/dt')

    plt.savefig(os.path.join(dirpath, 'time_difference_spike_plot.pdf'))
    plt.close()