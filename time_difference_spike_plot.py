import os
import numpy as np
import matplotlib.pyplot as plt

# put the file name with the dir location where the data is

dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\csv_output"
fname = "total_peaks_G2_tdiff_0p5mm_260mps_denatcyto7_2pstep_3cosfield_noRF_synced _sv_SDS_tstep1e2.csv"

data = np.genfromtxt(os.path.join(dirpath, fname), delimiter=',', skip_header=1)
gamma = data[:, 5]
toverdt = data[:, 7]

plt.plot(gamma, toverdt, color='black')
plt.xlabel('gamma')
plt.ylabel('t/dt')
plt.ylim((0, 100))
plt.xlim((0, 1.1))
plt.savefig(os.path.join(dirpath, 'time_difference_spike_plot.pdf'))
plt.close()