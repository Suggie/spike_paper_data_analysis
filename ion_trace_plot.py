import os
import numpy as np
import matplotlib.pyplot as plt

def get_trace_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith('ctrace') and x.endswith('.csv')]
    return files

def read_trace_file(dirpath, file):
    data = np.genfromtxt(os.path.join(dirpath, file), delimiter=',', skip_header=1)
    return data

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\082018\csv_output"
    trace_files = get_trace_files(dirpath)
    for file in trace_files:
        fig_fname = str(file).split(',')[0] + '.png'
        trace_data = read_trace_file(dirpath, file)
        plt.scatter(trace_data[:, 0], trace_data[:, 1], s=10, color='black')
        plt.xlim((51000, 52000))
        plt.ylim((0, 12))
        plt.savefig(os.path.join(dirpath, fig_fname), dpi=500)
        plt.close()
