import os
import numpy as np
import matplotlib.pyplot as plt


def plot_scatter_func(x, y, xlabel, ylabel, xlim_arr):
    plt.scatter(x, y, color='black')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim((xlim_arr[0], xlim_arr[1]))

def gamma(wavenumber, waveheight, wavevelocity, mobility):
    potential_amp = (16.67/40)*waveheight
    y = (wavenumber * mobility * potential_amp)/wavevelocity
    return y


if __name__=='__main__':
    cytcfile = r"C:\Users\sugyan\Documents\Papers\051318_SpikesPaper_withWaters\CytC_6+.txt_raw.csv_combined_gaussfit.csv"
    blacfile = r"C:\Users\sugyan\Documents\Papers\051318_SpikesPaper_withWaters\Blac_8+.txt_raw.csv_combined_gaussfit.csv"

    wavenumber = 524

    flist = [cytcfile, blacfile]
    # moblist = [0.02565920, 0.02499940]

    for file in flist:
        data = np.genfromtxt(file, delimiter=',', skip_header=1)
        wh, wv, mob, res = data[:, 0], data[:, 1], data[:, 2], data[:, 9]
        Voverv = np.divide(wh, wv)
        kVoverv = np.multiply(mob, Voverv)

        gamm_ = gamma(wavenumber, wh, wv, mob)

        dirp, dirn = os.path.split(file)
        fname_id = str(dirn.split('.')[0])
        plot_scatter_func(Voverv, res, 'V/v', 't/fwhm', [np.min(Voverv)-0.01, np.max(Voverv)+0.01])
        plt.savefig(os.path.join(dirp, fname_id+'_Voverv_vs_res.pdf'))
        plt.close()

        plot_scatter_func(kVoverv, res, 'KV/v', 't/fwhm', [np.min(kVoverv) - 0.001, np.max(kVoverv) + 0.001])
        plt.savefig(os.path.join(dirp, fname_id + '_KVoverv_vs_res.pdf'))
        plt.close()

        plot_scatter_func(gamm_, res, 'gamma', 't/fwhm', [np.min(gamm_) - 0.05, np.max(gamm_) + 0.05])
        plt.savefig(os.path.join(dirp, fname_id + '_gamma_vs_res.pdf'))
        plt.close()