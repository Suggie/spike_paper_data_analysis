import os
import numpy as np
import matplotlib.pyplot as plt

dirpath = r"C:\Users\sugyan\Documents\Processed data\070718_SPIKE_PAPER_DATA\csv_output"
fname = 'plateau_width_300mps_stepping.csv'

data = np.genfromtxt(os.path.join(dirpath, fname), delimiter=',', skip_header=8)
plateau_width = np.multiply(100, np.divide(data[:, 1], data[:, 2]))

arr_store = []

for ind, (plat_width, step_dist) in enumerate(zip(plateau_width, data[:, 0]*1.5)):
    if step_dist > 1.4:
        arr_store.append([step_dist, plat_width])
arr_store = np.asarray(arr_store)

plt.plot(arr_store[:, 0], arr_store[:, 1], ls='-', marker='o', markersize=8, mec='black', mfc='none', mew=1.75, color='black')
# plt.xlim((1.5, 3))
# plt.ylim((0, 6))
plt.xticks([1.5, 2.0, 2.5, 3.0], ('1.5', '2.0', '2.5', '3.0'))
plt.savefig(os.path.join(dirpath, 'dt_plateau_stepdistance_2.pdf'))
# plt.show()
plt.close()